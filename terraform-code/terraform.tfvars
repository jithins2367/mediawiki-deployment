region_name = "us-east-1"
project = "eks"
vpc_cidr_block = "10.0.0.0/16"
worker_node_types = ["t2.medium"]