variable "region_name" {
  type = string
}
variable "project" {
    type = string
}

variable "vpc_cidr_block" {
    type = string
}

variable "subnet_cidr_bits" {
  description = "The number of subnet bits for the CIDR. For example, specifying a value 8 for this parameter will create a CIDR with a mask of /24."
  type        = number
  default     = 8
}

variable "availability_zones_count" {
  description = "The number of AZs."
  type        = number
  default     = 2
}


variable "eks_cluster_version" {
  description = "EKS cluster version."
  type        = string
  default     = "1.23"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default = {
    "Project"     = "Terraform Demo"
    "Environment" = "Development"
    "Owner"       = "Jithin Scaria"
  }
}

variable "worker_node_types" {
  description = "EKS worker nodes machine types"
  type        = list(string)
  default     = ["t2.medium"]
}
