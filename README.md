## Mediawiki Application Deployment
## EKS Cluster Deployment

Terraform code used for deploying the cluster on AWS environment.

Used below terraform and linux commands used for deployment.

- 
  ```
  cd terraform-code/
  ```

- 
  ```
  terraform init
  ```
- 
  ```
  terraform plan
  ```
- 
  ```
  terraform apply --auto-approve
  ```

Create the storage class using the gp2.yaml

- 
  ```
  kind: StorageClass
  apiVersion: storage.k8s.io/v1
  metadata:
    name: gp2
    annotations:
      storageclass.kubernetes.io/is-default-class: "true"
  provisioner: kubernetes.io/aws-ebs
  parameters:
    type: gp2
    fsType: ext4 

  ```
- 
  ```
  kubectl create -f gp2.yaml
  ```

- Create IAM the IAM Policy
- Go to Services -> IAM
  Create a Policy
  Select JSON tab and copy paste the below JSON
  ```
  
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "ec2:AttachVolume",
          "ec2:CreateSnapshot",
          "ec2:CreateTags",
          "ec2:CreateVolume",
          "ec2:DeleteSnapshot",
          "ec2:DeleteTags",
          "ec2:DeleteVolume",
          "ec2:DescribeInstances",
          "ec2:DescribeSnapshots",
          "ec2:DescribeTags",
          "ec2:DescribeVolumes",
          "ec2:DetachVolume"
        ],
        "Resource": "*"
      }
    ]
  }
  ```

- Review the same in Visual Editor
- Click on Review Policy
  Name: Amazon_EBS_CSI_Driver
  Description: Policy for EC2 Instances to access Elastic Block Store
  Click on Create Policy

-  Get the IAM role Worker Nodes using and Associate this policy to that role
    ```
    # Get Worker node IAM Role ARN

    kubectl -n kube-system describe configmap aws-auth
    
    # from output check rolearn
    ```
- Go to Services -> IAM -> Roles
- Search for role with name eksctl-eksdemo1-nodegroup and open it
- Click on Permissions tab
  Click on Attach Policies
- Search for Amazon_EBS_CSI_Driver and click on Attach Policy
- Deploy Amazon EBS CSI Driver
  ```
  # Deploy EBS CSI Driver
  kubectl apply -k "github.com/kubernetes-sigs/aws-ebs-csi-driver/deploy/kubernetes/overlays/stable/?ref=master"

  Verify ebs-csi pods running
  kubectl get pods -n kube-system
  ```

# Jenkins Pipeline for Mediawiki Application deployment

  ```
  pipeline {
	  agent none
	  stages {
			stage("Mediawiki Container Build") {

				  agent any
				  steps {
					sh 'export DOCKER_BUILDKIT=0'
                    sh 'export COMPOSE_DOCKER_CLI_BUILD=0'
					sh 'docker build -t quay.io/jithinscaria236/mediawiki .'
			 
				} 
			}
		stage('Push Mediawiki Container Image to Quay Registry'){
			agent any
            steps{
               
                 
				   sh 'docker login -u="xxxxxx" -p="gxxxxeet/xxxxxxxxa7WjOG6A1/Wg==" quay.io'
                   sh 'docker push quay.io/jithinscaria236/mediawiki'
                
            }
        }
		stage('Create NameSpace in EKS Cluster'){
		    agent any
            steps{
                   sh 'kubectl create namespace mediawiki'
				  
                }
            }
		
		stage('Deploy MySQL Pod To EKS Cluster'){
		    agent any
            steps{
                   sh 'kubectl create -f mysql.yaml -n mediawiki'
				   
                }
            }
		stage('Deploy Mediawiki Pod To EKS Cluster'){
		    agent any
            steps{
                   sh 'kubectl create -f mediawiki.yaml -n mediawiki'
				   
                }
            }
	  }
	  
	}
	
	
  ```
  ![](images/media-wiki-jenkins.JPG)

- To view kubernetes pod status  and service details use below commands
  
  ```
  kubectl get pods -n mediawiki
  kubectl get svc -n mediawiki
  ```


  ![](images/pod-status.JPG)

  ![](images/media-wiki.png)

- Finally copy the LocalSettings.php to the mediawiki pod's document root
  ```
  kubectl cp LocalSettings.php mediawiki/mediawiki-587cc56b5-d7k75:/var/www/html
  ```

- Mediawiki final page
  
  ![](images/media-wiki-final.png)

- We can include any csv file as configmap and attach it to the volume.
  
# For setting up the resource quota , run below commands

- 
    ```
    cd resource-quota-yaml

    kubectl create -f quota.yaml
    ```

- To view the quota details for a namespace

  
  ```
  kubectl describe quota -n quota-namespace
  ```

# Future enhancements 

- Argocd implementation - for deployments
- Istio Service mesh -  For ingress controller implementation 
- RBAC for cluster access

